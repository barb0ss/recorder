import falcon
import json


# Falcon follows the REST architectural style, meaning (among
# other things) that you think in terms of resources and state
# transitions, which map to HTTP verbs.
class Records(object):
    def __init__(self, recorder):
        self.recorder = recorder

    def on_put(self, req, resp, stream_url):
        command = json.load(req.bounded_stream)
        self.recorder.exec_command(stream_url, command)

        resp.status = falcon.HTTP_200  # This is the default status
        resp.body = json.dumps(command)
