import sys
import gi

gi.require_version('Gst', '1.0')
gi.require_version('GLib', '2.0')

from gi.repository import Gst, GLib


option_silent = GLib.OptionEntry()
option_silent.arg = GLib.OptionArg.NONE
option_silent.arg_description = 'do not output status information'
option_silent.long_name = 'silent'
option_silent.short_name = ord('s')

option_output = GLib.OptionEntry()
option_output.arg = GLib.OptionArg.STRING
option_output.arg_description = 'save xml representation of pipeline to FILE and exit'
option_output.long_name = 'output'
option_output.short_name = ord('o')

entries = [option_silent, option_output]

ctx = GLib.OptionContext("- Your application")
ctx.add_main_entries(entries)
ctx.add_group(Gst.init_get_option_group())

try:
    ctx.parse(sys.argv)
except GLib.Error as e:
    print(f'Failed to initialize {e.message}')
    GLib.clear_error()
    ctx.free()
    exit(1)

ctx.free()

print('Run me with --help to see the Application options appended.\n')

exit(0)
