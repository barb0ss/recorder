import falcon
import gi
import threading

from api import Records
from recorder import Recorder

gi.require_version('GLib', '2.0')
gi.require_version('Gst', '1.0')

from gi.repository import Gst, GLib

recorder = Recorder()
records = Records(recorder)
api = falcon.API()
loop = GLib.MainLoop.new(None, False)

def start_loop():
    loop.run()

api.add_route('/records/{stream_url}/', records)

t = threading.Thread(target=loop.run)
t.start()
Gst.init()
# loop.run()
