import gi

gi.require_version('Gst', '1.0')

from gi.repository import Gst

Gst.init(None)

factory = Gst.ElementFactory.find('fakesrc')

if factory is None:
    print('fakesrc factory not found')
else:
    print('fakesrc factory was found')

element = factory.create('source')

if element is None:
    print('Failed to create element fakesrc')
else:
    print('Element fakersc created')
