import gi
import sys

gi.require_version('GLib', '2.0')
gi.require_version('Gst', '1.0')

from gi.repository import Gst, GLib


def bus_call(bus, message, loop):
    if message.type == Gst.MessageType.EOS:
        print('End of stream')
        loop.quit()
    elif message.type == Gst.MessageType.ERROR:
        error = message.parse_error()
        print(f'Error: {error.message}')
        loop.quit()

    return True


def on_pad_added(element, pad, decoder):
    print('Dynamic pad created, linking demuxer/decoder')

    sinkpad = decoder.get_static_pad('sink')
    pad.link(sinkpad)


def main():
    try:
        file_path = sys.argv[1]
    except IndexError:
        print(f'Usage: {sys.argv[0]} <Ogg/Vorbis filename>')
        exit(-1)

    loop = GLib.MainLoop.new(None, False)

    pipeline = Gst.Pipeline.new('audio-player')
    source = Gst.ElementFactory.make('filesrc', 'file-source')
    demuxer = Gst.ElementFactory.make('oggdemux', 'ogg-demuxer')
    decoder = Gst.ElementFactory.make('vorbisdec', 'vorbis-decoder')
    conv = Gst.ElementFactory.make('audioconvert', 'converter')
    sink = Gst.ElementFactory.make('autoaudiosink', 'audio-output')

    if not pipeline or not source or not demuxer or not decoder or not conv or not sink:
        print('One element could not be created. Exiting.')
        exit(-1)

    source.set_property('location', file_path)

    bus = pipeline.get_bus()
    bus_watch_id = bus.add_watch(GLib.PRIORITY_DEFAULT, bus_call, loop)

    pipeline.add(source)
    pipeline.add(demuxer)
    pipeline.add(decoder)
    pipeline.add(conv)
    pipeline.add(sink)

    source.link(demuxer)
    decoder.link(conv)
    conv.link(sink)

    demuxer.connect('pad-added', on_pad_added, decoder)

    print(f'Now playing: {sys.argv[1]}')

    pipeline.set_state(Gst.State.PLAYING)

    print('Running...')

    loop.run()

    print('Returned, stopping playback')

    pipeline.set_state(Gst.State.NULL)

    print('Deleting pipeline')


Gst.init()
main()
