import gi
import sys

gi.require_version('GLib', '2.0')
gi.require_version('Gst', '1.0')

from gi.repository import Gst, GLib


def bus_call(bus, message, loop):
    if message.type == Gst.MessageType.EOS:
        print('End of stream')
        loop.quit()
    elif message.type == Gst.MessageType.ERROR:
        error = message.parse_error()
        print(f'Error: {error[1]}')
        loop.quit()

    return True


def on_pad_added(element, pad, parser):
    print('Dynamic pad created, linking demuxer/decoder')

    sinkpad = parser.get_static_pad('sink')
    pad.link(sinkpad)


def main():
    try:
        url = sys.argv[1]
    except IndexError:
        print(f'Usage: {sys.argv[0]} RTMP stream url')
        exit(-1)

    loop = GLib.MainLoop.new(None, False)

    pipeline = Gst.Pipeline.new('rtmp-reciever')
    source = Gst.ElementFactory.make('rtmpsrc', 'rtmp-source')
    demuxer = Gst.ElementFactory.make('flvdemux', 'flv-demuxer')
    parser = Gst.ElementFactory.make('h264parse', 'h264-parser')
    muxer = Gst.ElementFactory.make('mp4mux', 'mp4-muxer')
    sink = Gst.ElementFactory.make('filesink', 'filesink')

    if None in [pipeline, source, demuxer, parser, muxer, sink]:
        print('One element could not be created. Exiting.')
        exit(-1)

    source.set_property('location', url)
    sink.set_property('location', 'stream.mp4')

    pipeline.add(source)
    pipeline.add(demuxer)
    pipeline.add(parser)
    pipeline.add(muxer)
    pipeline.add(sink)

    source.link(demuxer)
    muxer.link(sink)

    parser_src_pad = parser.get_static_pad('src')

    muxer_sink_pad = muxer.get_request_pad('video_1')

    parser_src_pad.link(muxer_sink_pad)

    demuxer.connect('pad-added', on_pad_added, parser)

    bus = pipeline.get_bus()
    bus.add_watch(GLib.PRIORITY_DEFAULT, bus_call, loop)

    print(f'Now playing: {sys.argv[1]}')

    pipeline.set_state(Gst.State.PLAYING)

    print('Running...')

    loop.run()

    pipeline.set_state(Gst.State.NULL)


Gst.init()
main()
