import gi
import uuid
import os.path
import playlist

gi.require_version('GLib', '2.0')
gi.require_version('Gst', '1.0')

from gi.repository import Gst, GLib


class Pipeline:
    def __init__(self, stream_url, pull_url, records_dir, on_stop=None):
        self._record_dir = os.path.join(records_dir, stream_url)
        self._playlist_location = os.path.join(self._record_dir, 'playlist.m3u8')
        self._stream_url = stream_url
        self._pull_url = pull_url
        self._on_stop = on_stop

        if not os.path.isdir(self._record_dir):
            os.mkdir(self._record_dir)

        self._last_running_time = 0
        self._pipeline = Gst.Pipeline.new('rtmp-reciever')
        self._source = Gst.ElementFactory.make('rtmpsrc', 'rtmp-source')
        self._demuxer = Gst.ElementFactory.make('flvdemux', 'flv-demuxer')
        self._parser = Gst.ElementFactory.make('h264parse', 'h264-parser')
        self._muxer = Gst.ElementFactory.make('mpegtsmux', 'mpegtsmux')
        self._sink = Gst.ElementFactory.make('splitmuxsink', 'splitsink')

        if None in [self._pipeline, self._source, self._demuxer, self._parser, self._sink]:
            print('One element could not be created. Exiting.')
            exit(-1)

        self._source.set_property('location', self._pull_url)

        self._sink.set_property('muxer-factory', 'mpegtsmux')
        self._sink.set_property('message-forward', True)
        self._sink.set_property('muxer', self._muxer)
        self._sink.set_property('reset-muxer', False)
        self._sink.set_property('send-keyframe-requests', True)
        self._sink.set_property('max-size-time', 12 * Gst.SECOND)
        self._sink.connect('format-location', self._on_format)

        self._pipeline.add(self._source)
        self._pipeline.add(self._demuxer)
        self._pipeline.add(self._parser)
        self._pipeline.add(self._sink)

        self._source.link(self._demuxer)

        parser_src_pad = self._parser.get_static_pad('src')

        splitmux_sink_pad = self._sink.get_request_pad('video')

        parser_src_pad.link(splitmux_sink_pad)

        self._demuxer.connect('pad-added', self._on_pad_added)

        bus = self._pipeline.get_bus()
        bus.add_watch(GLib.PRIORITY_DEFAULT, self._bus_call)

    def start(self):
        print(f'Now playing: {self._stream_url}')

        self._pipeline.set_state(Gst.State.PLAYING)

        print('Running...')

    def stop(self):
        source = self._pipeline.get_by_name('rtmp-source')
        source_src_pad = source.get_static_pad('src')
        source_src_pad.add_probe(Gst.PadProbeType.BLOCK, self._unlink)

        print(f'Now stopped: {self._stream_url}')

    def _on_pad_added(self, element, pad):
        print('Dynamic pad created, linking demuxer/decoder')
        print(f'Pad name={pad.get_name()}')

        if pad.get_name() == 'video':
            sinkpad = self._parser.get_static_pad('sink')
            pad.link(sinkpad)

    def _on_format(self, element, fragment_id):
        location = os.path.join(self._record_dir, f'{uuid.uuid4()}.ts')
        print(f'Next chunk location={location}')
        return location

    def _unlink(self, pad, info):
        self._pipeline.set_property('message-forward', True)

        demuxer_sink_pad = self._demuxer.get_static_pad('sink')

        pad.unlink(demuxer_sink_pad)

        demuxer_sink_pad.send_event(Gst.Event.new_eos())

        return Gst.PadProbeReturn.OK

    def _bus_call(self, bus, message):
        print(f'Message type={message.type} from elem={message.src.name}')

        if message.type == Gst.MessageType.EOS:
            self._pipeline.set_state(Gst.State.NULL)
            self._pipeline.set_property('message-forward', False)

            with playlist.open_(self._playlist_location) as pl:
                pl.end()

            if self._on_stop:
                self._on_stop(self._stream_url)

            print('End of stream')

        elif message.type == Gst.MessageType.ELEMENT:
            if message.has_name('splitmuxsink-fragment-closed'):
                structure = message.get_structure()
                running_time = structure.get_value("running-time")
                fragment_name = structure.get_value("location").split('/')[-1]
                duration = (running_time - self._last_running_time) / Gst.SECOND
                self._last_running_time = running_time

                with playlist.open_(self._playlist_location, 12) as pl:
                    url = f'http://127.0.0.1:8001/{self._stream_url}/{fragment_name}'
                    pl.add_entry(url, duration, 'live')

                print(f'fragment time={duration}')

        elif message.type == Gst.MessageType.ERROR:
            error = message.parse_error()
            print(f'Error: {error[1]}')

        return True
