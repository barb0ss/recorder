from pipeline import Pipeline


class Recorder:
    def __init__(self, records_dir):
        self._records_dir = records_dir
        self._pipelines = {}

    def exec_command(self, stream_url, command):
        if command.get('action') == 'start':
            self._start(stream_url, command.get('pull_url'))
        elif command.get('action') == 'stop':
            self._stop(stream_url)

    def _start(self, stream_url, pull_url):
        pipeline = self._pipelines.get(stream_url)

        if pipeline is None:
            pipeline = Pipeline(stream_url, pull_url, self._records_dir, self._on_pipeline_stop)
            self._pipelines[stream_url] = pipeline

        pipeline.start()

    def _stop(self, stream_url):
        pipeline = self._pipelines.get(stream_url)

        if pipeline:
            pipeline.stop()

    def _on_pipeline_stop(self, stream_url):
        self._pipelines.pop(stream_url)
