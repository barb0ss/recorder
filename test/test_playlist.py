from unittest import TestCase

import playlist

import os


class TestPlaylist(TestCase):
    def test_playlist_ctx_manager(self):
        playlist_path = 'test_playlist'

        with playlist.open_(playlist_path, 12) as pl:
            self.assertIsInstance(pl, playlist.PlayList)

        os.remove(playlist_path)

    def test_open_playlist_with_header(self):
        playlist_path = 'test_playlist'

        with playlist.open_(playlist_path, 12) as pl:
            content = pl.read()
            self.assertTrue(len(content) > 0)

        os.remove(playlist_path)

    def test_set_media_sequence(self):
        playlist_path = 'test_playlist'

        with playlist.open_(playlist_path, 12) as pl:
            before = pl.get_sequence()
            self.assertEqual(before, 0)

            pl.set_sequence(5)
            after = pl.get_sequence()
            self.assertEqual(after, 5)

        os.remove(playlist_path)

