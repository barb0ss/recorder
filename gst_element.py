import gi

gi.require_version('Gst', '1.0')

from gi.repository import Gst

Gst.init(None)

element = Gst.ElementFactory.make('fakesrc', 'source')

if element is None:
    print('Failed to create element fakesrc')
else:
    print('Element fakersc created')
