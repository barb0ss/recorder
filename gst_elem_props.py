import gi

gi.require_version('Gst', '1.0')

from gi.repository import Gst

Gst.init(None)

element = Gst.ElementFactory.make('fakesrc', 'source')
name = element.get_name()

print(f'The name of the element is {name}')
