import gi

gi.require_version('GLib', '2.0')
gi.require_version('Gst', '1.0')

from gi.repository import Gst, GLib


def bus_callback(bus, message, data=None):
    if message.type == Gst.MessageType.ERROR:
        error = message.parse_error()
        print(f'Error {error.message}')

        GLib.MainLoop.quit()
    elif message.type == Gst.MessageType.EOS:
        GLib.MainLoop.quit()

    return True


Gst.init(None)


pipeline = Gst.Pipeline.new('my_pipeline')

bus = pipeline.get_bus()
bus.add_watch(1, bus_callback, None)

loop = GLib.MainLoop.new(None, False)
loop.run()
