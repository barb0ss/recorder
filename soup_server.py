import gi
import json
import re
import os

from recorder import Recorder

gi.require_version('GLib', '2.0')
gi.require_version('Gst', '1.0')
gi.require_version('Soup', '2.4')
gi.require_version('Gio', '2.0')

from gi.repository import Gst, GLib, Soup, Gio


pattern = re.compile(r'/records/(?P<stream_url>\w+)')
recorder = Recorder(os.getcwd())


def handler(server, msg, path, query, client):
    m = pattern.match(path)

    try:
        stream_url = m.group("stream_url")
    except AttributeError:
        return msg.set_status_full(Soup.Status.NOT_FOUND, 'Not found')

    if msg.method == 'PUT':
        command = json.loads(msg.request_body.data)
        recorder.exec_command(stream_url, command)
        msg.set_status(Soup.Status.OK)
        return msg.set_response('application/json; charset=utf-8', Soup.MemoryUse.COPY, json.dumps(command).encode('utf-8'))

    return msg.set_status_full(Soup.Status.METHOD_NOT_ALLOWED, 'Method Not Allowed')


server = Soup.Server()
address = Gio.InetSocketAddress.new_from_string('127.0.0.1', 8000)
loop = GLib.MainLoop.new(None, False)

server.add_handler('/records', handler)

server.listen(address, 0)

Gst.init()

loop.run()
