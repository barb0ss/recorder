import os
import mmap
import re

from contextlib import contextmanager


@contextmanager
def open_(path, duration=None, type_='EVENT', version=3):
    playlist = None

    try:
        playlist = PlayList(path, duration=duration, type_=type_, version=version)

        yield playlist
    finally:
        if playlist:
            playlist.close()


_tmpl = '#EXTM3U\n' \
        '#EXT-X-PLAYLIST-TYPE:{type}\n' \
        '#EXT-X-VERSION:{version}\n' \
        '#EXT-X-MEDIA-SEQUENCE:{sequence}\n' \
        '#EXT-X-TARGETDURATION:{duration}\n'

_entry_tmpl='#EXTINF:{duration},{description}\n{url}\n'

_media_sequence_pattern = re.compile(rb'#EXT-X-MEDIA-SEQUENCE:(?P<seq>\d+)')

_end_list_pattern = re.compile(rb'#EXT-X-ENDLIST')


def _open_playlist(path):
    return open(path, 'r+')


def _create_playlist(path, duration, sequence, type_, version):
    pl = open(path, 'w+')

    pl.write(_tmpl.format(duration=duration, sequence=sequence, type=type_, version=version))

    pl.seek(0, os.SEEK_SET)

    return pl


class PlayList:
    def __init__(self, path, duration, type_, version):
        self._sequence = 0

        if os.path.isfile(path):
            self._file = _open_playlist(path)
        else:
            self._file = _create_playlist(path, duration, self._sequence, type_, version)

        self._mm = mmap.mmap(self._file.fileno(), 0)

    def close(self):
        self._mm.close()
        self._file.close()

    def read(self):
        self._file.seek(0, os.SEEK_SET)
        return self._file.read()

    def add_entry(self, url, duration, description=''):
        m = _end_list_pattern.search(self._mm)

        if m:
            self._file.seek(m.start())
        else:
            self._file.seek(0, os.SEEK_END)

        self._file.write(_entry_tmpl.format(url=url, duration=duration, description=description))

    def end(self):
        self._file.seek(0, os.SEEK_END)
        self._file.write('#EXT-X-ENDLIST')

    def set_sequence(self, value):
        m = _media_sequence_pattern.search(self._mm)

        if m:
            encoded_value = str(value).encode('utf-8')
            lower_bound = m.start('seq')
            upper_bound = m.start('seq') + len(encoded_value)

            self._mm[lower_bound:upper_bound] = encoded_value

    def get_sequence(self):
        m = _media_sequence_pattern.search(self._mm)

        if m:
            return int(m.group('seq'))
