import gi

gi.require_version('Gst', '1.0')

from gi.repository import Gst

Gst.init(None)

factory = Gst.ElementFactory.find('fakesrc')

if factory is None:
    print('You don\'t have the "fakesrc" element installed')


element_name = factory.get_name()
element_category = factory.get_metadata(Gst.ELEMENT_METADATA_KLASS)
element_description = factory.get_metadata(Gst.ELEMENT_METADATA_DESCRIPTION)

print(f'The {element_name} element is a member of category {element_category}.\nDescription: {element_description}')
