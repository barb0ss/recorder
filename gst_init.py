import gi

gi.require_version('Gst', '1.0')

from gi.repository import Gst

Gst.init()

major, minor, micro, nano = Gst.version()

print(f'This program is linked against GStreamer {major}.{minor}.{micro}.{nano}')
